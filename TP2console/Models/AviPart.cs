﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP2console.Models.EntityFramework
{
    public partial class Avi
    {
        public override string ToString()
        {
            return this.Utilisateur.ToString()+" - "+this.Film.ToString()+" \n Avis :  " + this.Avis;
        }
    }
}
