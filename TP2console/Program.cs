﻿using System;
using System.Collections.Generic;
using System.Linq;
using TP2console.Models.EntityFramework;

namespace TP2console
{
    class Program
    {
        static void Main(string[] args)
        {
            /*using ( var ctx = new FilmsDBContext())
            {
                // Requête SELECT
                Film titanic = ctx.Films.First(f => f.Nom.Contains("Titanic"));

                // Modification de l'entité ( dans le contexte seulement )
                titanic.Description = "un bateau échoué. Date : " + DateTime.Now;

                // Sauvegarde du Contexte => Application de la modification dans la BD
                int nbchanges = ctx.SaveChanges();

                Console.WriteLine("Nombre d'enregistrement modifiés ou ajoutés : " + nbchanges);
            }*/

            AddAvis();
            Console.ReadKey();
        }

        public static void Exo2Q1()
        {
            var ctx = new FilmsDBContext();
            foreach (var film in ctx.Films)
            {
                Console.WriteLine(film.ToString());
            }
        }

        public static void Exo2Q2()
        {
            var ctx = new FilmsDBContext();
            foreach (var user in ctx.Utilisateurs)
            {
                Console.WriteLine(user.Email);
            }
        }

        public static void Exo2Q3()
        {
            var ctx = new FilmsDBContext();
            var users = ctx.Utilisateurs.OrderBy(c => c.Login);
            foreach (var user in users)
            {
                Console.WriteLine(user.ToString());
            }
        }

        public static void Exo2Q4()
        {
            var ctx = new FilmsDBContext();
            var films = ctx.Films.Where(f => f.CategorieNavigation.Nom == "Action");
            foreach (var film in films)
            {
                Console.WriteLine("ID " + film.Id + " : " + film.Nom);
            }
        }

        public static void Exo2Q5()
        {
            var ctx = new FilmsDBContext();
            var nbCategorie = ctx.Categories.Count();
            Console.WriteLine("Nombre de catégories différentes : " + nbCategorie);
        }

        public static void Exo2Q6()
        {
            var ctx = new FilmsDBContext();
            double minNote = ctx.Avis.Min(c => c.Note);
            Console.WriteLine("Note la plus basse : " + minNote);
        }

        public static void Exo2Q7()
        {
            var ctx = new FilmsDBContext();
            var films = ctx.Films.Where(f => f.Nom.ToLower().StartsWith("LE".ToLower()));
            foreach (var film in films)
            {
                Console.WriteLine(film.ToString());
            }
        }

        public static void Exo2Q8()
        {
            var ctx = new FilmsDBContext();
            var moyenne = ctx.Avis.Where(a => a.FilmNavigation.Nom.ToLower() == "pUlP Fiction".ToLower()).Average(a => a.Note);
            Console.WriteLine("La moyenne des notes sur Pulp Fiction est de : " + moyenne);
        }

        public static void Exo2Q9()
        {
            var ctx = new FilmsDBContext();
            var user = ctx.Utilisateurs.Where(u => u.Id == ctx.Avis.Where(a => a.Note == ctx.Avis.Max(av => av.Note)).Single().UtilisateurNavigation.Id).Single();
            // ctx.Avis.Where(a => a.Note == ctx.Avis.Max(av => av.Note)).Single();
            Console.WriteLine("L'utilisateur avec la plus grosse note est : " + user.Login);
        }

        public static void AddUser()
        {
            var ctx = new FilmsDBContext();
            ctx.Utilisateurs.Add(new Utilisateur
            {
                Login = "aymrick",
                Pwd = "root",
                Email = "aymrick.cougoulat@gmail.com"
            });
            int nbchanges = ctx.SaveChanges();
            Console.WriteLine("Nombre d'enregistrements modifiés ou ajoutés : " + nbchanges);
        }

        public static void EditFilm()
        {
            var ctx = new FilmsDBContext();
            Film film = ctx.Films.First(f => f.Nom.Contains("L'armee des douze singes"));
            film.Description = "Voici la nouvelle description du film";
            film.Categorie = ctx.Categories.First(c => c.Nom.Contains("Drame")).Id;
            int nbchanges = ctx.SaveChanges();
            Console.WriteLine("Nombre d'enregistrements modifiés ou ajoutés : " + nbchanges);
        }

        public static void DeleteFilm()
        {
            var ctx = new FilmsDBContext();
            Film film = ctx.Films.First(f => f.Nom.Contains("L'armee des douze singes"));

            var avis = ctx.Avis.Where(a => a.FilmNavigation.Id == film.Id);
            foreach (var avi in avis)
            {
                ctx.Avis.Remove(avi);
            }
            ctx.Films.Remove(film);

            int nbchanges = ctx.SaveChanges();
            Console.WriteLine("Nombre d'enregistrements modifiés ou ajoutés : " + nbchanges);
        }

        public static void AddAvis()
        {
            var ctx = new FilmsDBContext();
            Utilisateur user = ctx.Utilisateurs.First(u => u.Login.Contains("aymrick"));
            Film film = ctx.Films.First(f => f.Nom.Contains("Alien"));

            Avi avi = new Avi() { Avis = "Connais po", Note = 0.22528926479876474f, Film = film.Id, Utilisateur = user.Id };

            ctx.Avis.Add(avi);
            int nbchanges = ctx.SaveChanges();
            Console.WriteLine("Nombre d'enregistrements modifiés ou ajoutés : " + nbchanges);
        }

        public static void AddDrameFilm()
        {
            var ctx = new FilmsDBContext();
            Categorie catDrame = ctx.Categories.First(c => c.Nom.Contains("Drame"));

            var films = new List<Film>() { };
            films.Add(new Film() { Categorie = catDrame.Id, Nom = "Kaamelot", Description = "le retour du roi Arthur" });
            films.Add(new Film() { Categorie = catDrame.Id, Nom = "Kaamelot 2", Description = "le re retour du roi Arthur" });

            ctx.Films.AddRange(films);

            int nbchanges = ctx.SaveChanges();
            Console.WriteLine("Nombre d'enregistrements modifiés ou ajoutés : " + nbchanges);
        }
    }
}
